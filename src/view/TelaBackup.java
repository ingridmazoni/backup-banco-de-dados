package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Button;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import dao.*;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.AbstractListModel;
import javax.swing.ListSelectionModel;
import pojo.*;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
public class TelaBackup extends JFrame {

	private JPanel contentPane;
	private JList listDatabases;
	private DefaultListModel modelo;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaBackup frame = new TelaBackup();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaBackup() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				preencheListaDatabases();
			}
		});
		setResizable(false);
		setTitle("Backup");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 685, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnSalvarArquivo = new JButton("Backup de Todas os Databases");
		btnSalvarArquivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				backupAllDatabases();
			}
		});
		btnSalvarArquivo.setBounds(59, 48, 295, 23);
		contentPane.add(btnSalvarArquivo);
		
		JButton btnBackupDasDatabases = new JButton("Backup das Databases Selecionadas");
		btnBackupDasDatabases.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				backupOneDatabase();
			}
		});
		btnBackupDasDatabases.setBounds(298, 155, 288, 23);
		contentPane.add(btnBackupDasDatabases);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(72, 106, 167, 196);
		contentPane.add(scrollPane);
			
		modelo=new DefaultListModel();
			/*String[] values = new String[] {};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		};*/
		 //listDatabases.setModel(modelo);
		
		listDatabases = new JList(modelo);
		listDatabases.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		scrollPane.setViewportView(listDatabases);
			
	}
	
	public void backupAllDatabases(){
		JFileChooser jfile=new JFileChooser("C:/");
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Backup de Databases *.bak","bak");
		jfile.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		jfile.setAcceptAllFileFilterUsed(false);
		jfile.setFileFilter(filter);
		File caminho=null;
			

	    int retorno = jfile.showSaveDialog(null); 
	    if (retorno==JFileChooser.APPROVE_OPTION){
	          caminho = jfile.getSelectedFile(); 
	          System.out.println(caminho.getAbsolutePath());
	    }
	      
	    try {
	    	if(caminho.isDirectory()){
	    		BackupDao backdao=new BackupDao();
				backdao.backupDatabases(caminho.getAbsolutePath());
				JOptionPane.showMessageDialog(null, "O backup de todas as databases foi efetuado com sucesso");
	    	}
	    	else{
	    		JOptionPane.showMessageDialog(null,"Diretorio selecionado � inv�lido");
	    	}
			
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
	  
	}
	
	
	public void backupOneDatabase(){
	    JFileChooser chooser = new JFileChooser("C:/");
	    FileNameExtensionFilter filter = new FileNameExtensionFilter("Backup de Databases *.bak","bak");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.setFileFilter(filter);
		
		
	    File caminho;
	    
	    int retorno = chooser.showSaveDialog(null);
	    if (retorno==JFileChooser.APPROVE_OPTION){
	          caminho = chooser.getSelectedFile(); 
	          String temp="";
	          
	          if(caminho.isDirectory()){
	        	  ArrayList<String>databases=listaDatabasesSelecionadas();
		          BackupDao bd=new BackupDao();
		         
		  			for(String database:databases){
		  				bd.backupDatabase(database, caminho.getAbsolutePath());
		  				temp=temp+database+"\n";
		  			}
		  			JOptionPane.showMessageDialog(null, "O backup das databases selecionadas foi realizado com sucesso\n"+temp);
	          }
	          else{
	        	  JOptionPane.showMessageDialog(null, "A pasta selecionada � inv�lida");
	          }
	     }
	  }
	
	
	public ArrayList<String> listaDatabasesSelecionadas(){
		ArrayList<String> databases=new ArrayList<String>();
		
		
		for(int i=0;i<modelo.getSize();i++){
			if(listDatabases.isSelectedIndex(i)){
				databases.add(String.valueOf(modelo.getElementAt(i).toString()));
				System.out.println(modelo.getElementAt(i).toString());
			}
		}
		
		return databases;
		
	}
	
	
	public void preencheListaDatabases(){
		if(modelo.getSize()!=0){
			listDatabases.removeAll();
		}
		List<Database> lista=listaDatabase();
		
			if(lista!=null){
				for(Database database:lista){
					if(!database.getNome().equals("tempdb")){
						modelo.addElement(database.getNome());
					}
				}
			}
		
	}
	
	

	
	public List<Database> listaDatabase(){
		BackupDao autoDao=new BackupDao();
		List<Database> lista=new ArrayList<Database>();
		lista=autoDao.consultaListaDatabase();
		return lista;
	}
}
