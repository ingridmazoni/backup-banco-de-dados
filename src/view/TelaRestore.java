package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import dao.*;
import pojo.*;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class TelaRestore extends JFrame {

	private JPanel contentPane;
	private JTable tabelaDatabases;
	private DefaultTableModel modelo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaRestore frame = new TelaRestore();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaRestore() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				atualizaTabela();
			}
		});
		setTitle("Restore Database");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 582, 376);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnRestaurarDatabase = new JButton("Restaurar Database");
		btnRestaurarDatabase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				restoreOneDatabase();
			}
		});
		btnRestaurarDatabase.setBounds(46, 59, 185, 23);
		contentPane.add(btnRestaurarDatabase);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(70, 194, 413, 120);
		contentPane.add(scrollPane);
		
		tabelaDatabases = new JTable();
		modelo=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nome Database", "Data Cria\u00E7\u00E3o", "Hora"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		};
		tabelaDatabases.setModel(modelo);
		tabelaDatabases.getColumnModel().getColumn(0).setPreferredWidth(210);
		tabelaDatabases.getColumnModel().getColumn(1).setPreferredWidth(128);
		tabelaDatabases.getColumnModel().getColumn(2).setPreferredWidth(215);
		scrollPane.setViewportView(tabelaDatabases);
		
		JButton btnRestaurarTodasAs = new JButton("Restaurar Databases Selecionadas");
		btnRestaurarTodasAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				restoreAllDatabases();
			}
		});
		btnRestaurarTodasAs.setBounds(293, 59, 211, 23);
		contentPane.add(btnRestaurarTodasAs);
		
		JButton btnRestaurarTodasAs_1 = new JButton("Restaurar Todas as Databases 2");
		btnRestaurarTodasAs_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				restoreAllDatabases2();
			}
		});
		btnRestaurarTodasAs_1.setBounds(137, 134, 222, 23);
		contentPane.add(btnRestaurarTodasAs_1);
	}
	
	
	public void restoreOneDatabase(){
		
		JFileChooser jfile=new JFileChooser("C:/");
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Restore de Databases *.bak","bak");
		jfile.setFileSelectionMode(JFileChooser.FILES_ONLY);
		jfile.setAcceptAllFileFilterUsed(false);
		jfile.setFileFilter(filter);
		jfile.setEnabled(false);
		File database=null;
		String nomeDatabase="";
		
		
		   int retorno = jfile.showOpenDialog(null); 
		    if (retorno==JFileChooser.APPROVE_OPTION){
		           database= jfile.getSelectedFile(); 
		           nomeDatabase=database.getName().substring(0,database.getName().lastIndexOf("."));
		           
		           if(database.isFile()){
		        	   RestoreDao rd=new RestoreDao();
					    if(rd.verificaDatabase(nomeDatabase)==1){
					    	if(JOptionPane.showConfirmDialog(null, "A database j� existe.Se continuar a restaura��o a database ser� sobreescrita","Aviso",JOptionPane.OK_CANCEL_OPTION,JOptionPane.WARNING_MESSAGE)==JOptionPane.OK_OPTION){
					    		rd.restoreDatabases(nomeDatabase,database.getAbsolutePath());
					    		JOptionPane.showMessageDialog(null, "A database "+nomeDatabase+" foi restaurada com sucesso");
					    	}
					    }
					    else{
					    	rd.restoreDatabases(nomeDatabase, database.getAbsolutePath());
					    	JOptionPane.showMessageDialog(null, "A database "+nomeDatabase+" foi restaurada com sucesso");
					    }
					    
					    atualizaTabela();  
		           }
		           else{
		        	   JOptionPane.showMessageDialog(null, "O arquivo selecionado � inv�lido");
		           }
		          
		     }
	}
	
	
	public void restoreAllDatabases(){
		JFileChooser jfile=new JFileChooser("C:/");
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Restore de Databases *.bak","bak");
		jfile.setFileSelectionMode(JFileChooser.FILES_ONLY);
		jfile.setAcceptAllFileFilterUsed(false);
		jfile.setFileFilter(filter);
		jfile.setMultiSelectionEnabled(true);
				
		   int retorno = jfile.showOpenDialog(null); 
		    if (retorno==JFileChooser.APPROVE_OPTION){
		         File arquivos[] = jfile.getSelectedFiles(); 
		         String temp="";   
		         
		          for(File f:arquivos){
		        	  if(f.isFile()){
		        		  RestoreDao rd=new RestoreDao();
			        	  rd.restoreDatabases(f.getName().substring(0, f.getName().lastIndexOf(".")), f.getAbsolutePath());
			        	  temp=temp+f.getName()+"\n";
			        	}
		        	  else{
		        		  JOptionPane.showMessageDialog(null, "O arquivo "+f.getName()+" n�o � v�lido");
		        	  }
		        	}
		          
		          if(temp!=""){
		        	  JOptionPane.showMessageDialog(null,"As databases selecionadas foram restauradas com exito\n"+temp);
		        	  atualizaTabela();   
		          }
		          
		    }
		    
		 }
	
	
	public List<Database> listaDatabase(){
		BackupDao autoDao=new BackupDao();
		List<Database> lista=new ArrayList<Database>();
		lista=autoDao.consultaListaDatabase();
		return lista;
	}
	
	
	public void atualizaTabela(){
		List<Database> lista=listaDatabase();
		
		if(modelo.getRowCount()!=0){
			modelo.setRowCount(0);
		}
		if(lista!=null){
			for(Database p:lista){
				Object[] objeto=new Object[3];
				objeto[0]=p.getNome();
				objeto[1]=p.getData();
				objeto[2]=p.getHora();
				modelo.addRow(objeto);
				
			}
		}
		
	}
	
	
	public void restoreAllDatabases2(){
		JFileChooser jfile=new JFileChooser("C:/");
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Restore de Databases *.bak","bak");
		jfile.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		jfile.setAcceptAllFileFilterUsed(false);
		jfile.setFileFilter(filter);
		jfile.setMultiSelectionEnabled(true);
				
		   int retorno = jfile.showOpenDialog(null); 
		    if (retorno==JFileChooser.APPROVE_OPTION){
		    	
		    	File diretorio=new File(jfile.getSelectedFile().getAbsolutePath());
		    			    		
		         FilenameFilter ff=new FilenameFilter(){
		        	@Override
					public boolean accept(File b,String name) {
						return name.endsWith(".bak");
					}
		         };
		         
		        if(diretorio.isDirectory()){
		        	 File[] arquivos=diretorio.listFiles(ff);
		        	 		        	 
		        	 if(arquivos.length!=0){
		        		 String temp="";	 
				    	   for(File f:arquivos){
				    		   	
					        	 RestoreDao rd=new RestoreDao();
					        	 rd.restoreDatabases(f.getName().substring(0, f.getName().lastIndexOf(".")), f.getAbsolutePath());
					        	 temp=temp+f.getName()+"\n";
				        	}
				    	   JOptionPane.showMessageDialog(null, "As databases abaixo foram restauradas com exito\n"+temp);
				    	   atualizaTabela();
					  }
		        	 else{
		        		 JOptionPane.showMessageDialog(null, "A pasta n�o contem nenhum arquivo com extens�o .bak");
		        	 }
		        	 
		        }	 
		        else{
		        	JOptionPane.showMessageDialog(null,"A pasta selecionada � inv�lida");	 
		        }
		      
		     
		    }
	}
	
	
	
	
}
