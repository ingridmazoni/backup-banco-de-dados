package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class RestoreDao {
Connection c;	

	public RestoreDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public int verificaDatabase(String database){
		int i=0;
		String sql="select count(*) as registro from sys.sysdatabases where name like ?";
				
		try {
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, database);
			
			ResultSet rs=ps.executeQuery();
			
			
			if(rs.next()){
				i=rs.getInt("registro");
						
			}
			rs.close();
			ps.close();
			
			 
			
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
		return i;
		
	}
	
	public boolean restoreDatabases(String nome,String caminho){
		boolean inserido=false;
		String sql="{call restoreDatabase(?,?)}";
		
			CallableStatement cs;
			try {
				cs = c.prepareCall(sql);
				cs.setString(1, nome);
				cs.setString(2,caminho);
				
				cs.execute();
				cs.close();
				inserido=true;
			} catch (SQLException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);	
			}
			
		return inserido;
		
	}
	
	

}
