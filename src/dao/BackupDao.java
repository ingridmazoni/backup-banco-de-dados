package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import pojo.*;

public class BackupDao {
	Connection c;
	
	public BackupDao() {
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	
	public boolean backupDatabases(String caminho)throws SQLException{
		boolean inserido=false;
		String sql="{call fullbackupAll(?)}";
		
			CallableStatement cs=c.prepareCall(sql);
			cs.setString(1,caminho);
			cs.execute();
			cs.close();
			inserido=true;
		return inserido;
		
	}
	
	
	public List<Database> consultaListaDatabase(){
		List<Database> listaDatabase=new ArrayList<Database>();
		Database database=null;
		
		StringBuffer sql=new StringBuffer();
		sql.append("select * from listaDatabases order by data,hora");
		
		
		 try {
			 	database=new Database();
			 	PreparedStatement ps=c.prepareStatement(sql.toString());
				ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				Database DatabaseConsultado=new Database();
				DatabaseConsultado.setNome(rs.getString("name"));
				DatabaseConsultado.setData(rs.getString("data"));
				DatabaseConsultado.setHora(rs.getString("hora"));
				listaDatabase.add(DatabaseConsultado);
			}
			
			rs.close();
			ps.close();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaDatabase;
		 
	}
	
	
	public boolean backupDatabase(String nome,String caminho){
		boolean inserido=false;
		String sql="{call fullbackupOne(?,?)}";
		
			CallableStatement cs;
			try {
				cs = c.prepareCall(sql);
				cs.setString(1,nome);
				cs.setString(2,caminho);
				cs.execute();
				cs.close();
				inserido=true;
				
			} catch (SQLException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			}
			return inserido;	
		
	}

}
