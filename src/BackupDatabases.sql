create database copia
use copia
backup database copia to disk='C:\databases3\copia.bak'
restore database copia from disk='C:\databases3\copia.bak' with recovery, replace


create procedure fullbackupAll(@caminho varchar(100))
as
	declare @name as varchar(100)
	declare @crdate as date
	declare @query as varchar(max)


	declare backupdatabases cursor for
	select name,crdate from sys.sysdatabases

	open backupdatabases
	fetch next from backupdatabases
	into @name,@crdate
	
	
	while @@FETCH_STATUS=0
	begin
		if(@name!='tempdb')
		begin
			set @query='backup database '+ @name +' to disk = '''+@caminho+'\'+@name+'.bak'''
			exec(@query)
		end
		
	fetch next from backupdatabases
	into @name,@crdate
		
	end
	close backupdatabases
	deallocate backupdatabases
	
	
exec fullbackupAll 'c:\databases3\'


create procedure fullbackupOne(@nome varchar(100),@caminho varchar(100))
as
		declare @query as varchar(max)
		if(@nome!='tempdb')
		begin
			set @query='backup database '+ @nome +' to disk = '''+@caminho+'\'+@nome+'.bak'''
			print @query
			exec(@query)
		end
		
	
	exec fullbackupOne 'saude','C:\databases3'
	backup database saude to disk='C:\databases3'
	

	create view listaDatabases as
	select name,
	cast(DATEPART(day,crdate) as varchar(2))+'/'+
	cast(DATEPART(month,crdate)as varchar(2))+'/'+
	cast(DATEPART(year,crdate)as varchar(4)) as data,
	CAST(datepart(hour,crdate)as varchar(2))+':'+
	CAST(datepart(minute,crdate)as varchar(2))as hora
	from sys.sysdatabases
	
	
	select * from listaDatabases order by data,hora
	
	
	declare @name1 as varchar(100)
	declare @caminho1 as varchar(100)
	declare @query1 as varchar(max)
	set @name1='bdteste'
	set @caminho1='C:\databases2'
	
	set @query1='backup database '+ @name1 +' to disk = '''+@caminho1+'\'+@name1+'.bak'''
	print @query1
	
	
	create procedure restoreDatabase(@nome as varchar(100),@caminho as varchar(100))
	as
	declare @query as varchar(max)
	set @query='restore database '+ @nome+' from disk='''+@caminho+'''with recovery, replace '
	exec(@query)
	
		
	exec restoreDatabase 'formula1','C:\databases2\formula1.bak'
	

	